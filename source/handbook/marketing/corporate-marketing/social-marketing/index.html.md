---
layout: handbook-page-toc
title: "Social Marketing Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Social Marketing Handbook
*The social marketing team is responsible for the stewardship of the GitLab brand social channels. We’re accountable for the organic editorial calendar and work with partners across the community advocacy, digital marketing, talent brand, and other teams to orchestrate the social media landscape for GitLab.*

## Requesting Social Posts <a name="requesting social promotion"></a>

### Open a new issue to request social coverage
- Head to the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues) and create a new issue.
- Select the appropriate issue template:
   - **social-event-request** for coverage of events (tradeshows, etc.)
   - **social-team-advocacy** when the project calls for GitLab Team Members to support our campaign efforts on their personal social channels
   - **social-general-request** for every other request
- Fill out as much information as you can above the first line in the issue.

### Please remember...
- For an anything to get promoted on social, **there must be a dedicated social issue**.
- If the need is urgent, send a message to the `#social_media` Slack Channel.
- If you have already requested or received images for social (paid) ads, please mark that issue as related in the organic social request issue.
- If you have not already requested or received images: Shortly after you open your social issue, the social team will assess whether there are existing assets we can use on social, or if new ones are needed. They will then request new images from the design team, or remove them from the issue. It is at the design teams' discretion whether they have time to create the images, particularly if you open your issue within ~ 1 week of the need to publish.
- Sometimes it is not possible to schedule posts when desired due to any number of reasons, but the social team will work with you to make sure you're supported.
- The social team reserves the right to not publish for a myriad of reasons including crisis moments, calendar priorities, and other elements. We'll do our best to explain why when asked.

## Social Takeover Notes <a name="social takeover notes"></a>

So you've been nominated for a short-term takeover of GitLab's social channels — here's a quick guide with everything you need to know.

**Format:**
* Create [tracked links](https://docs.google.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit#gid=0) for every link back to about.gitlab.com (e.g., all blog posts, landing pages, etc.)
* Publish and schedule in Sprout, using the “shorten links” button
* Choose a tag for every post that goes out (there is a dropdown)
* Preview posts in Sprout to make sure spacing, image, etc. looks fine on desktop and mobile

**Timing:**
* **Check the schedule in Sprout.**
* Do not publish more than 1-2 times per day on Facebook & LinkedIn (studies show > 2 posts per day on brand pages on these platforms starts to feel spammy).
* Do not publish tweets within 30 minutes of each other. Retweets of other stuff within that time is fine.
* Especially if someone reacts to an article that you tweeted with a substantive comment, question, or conversation starter, that is a good one to retweet promptly, because it could spur additional engagement.
* If you’re debating between posting early and posting late, post early (i.e., 4 am Pacific). We have a sizable audience in Europe, so it’s best to resist posting after 4 pm PT (unless it’s a retweet, or a post that is newsworthy and published late)

**Content:**
* Newly published posts
* Retweet of substantive, high quality tweets, especially from team members
* Archive posts
   * If you feel the day has been light on content, feel free to check out [sample copy & assets here](https://docs.google.com/spreadsheets/d/1_RdHsIflAdLjvZKzCzMCa-xBgwZYoZgGBJr0E-6Djcs/edit?usp=sharing).
* Can also fill some space in the afternoon with a job posting (“jobs of the week” are in the [company call agenda](https://docs.google.com/document/d/1JiLWsTOm0yprPVIW9W-hM4iUsRxkBt_1bpm3VXV4Muc/edit) doc)
* Have fun with it! Be liberal with emojis.
* Tip: Whereas on many other channels we always want a clear CTA, don't feel bound to this when writing for Twitter. Instead of always writing in the formation `Interested in x? Click on y!`, try to think about Twitter as a publishing channel rather than just a distribution channel. Try thinking about your tweet as a micro blog post: include the main thrust of the thing you're promoting, or write any funny or intriguing details that accent the information displayed on the twitter card. 
      * For example, [this tweet](https://twitter.com/gitlab/status/975463098676076544) about burnout is one of our most popular of 2018. 
      * We could have written something like "Here's what GitLab team-members have to say about burnout:" We do that plenty, but it's tedious if overused. 
      * See what we did instead, and note how you can include incomplete sentences and details that will supplement, rather than duplicate, the information in the blog post title and description.

![tweet example](/images/handbook/marketing/tweet-example.png){:.shadow}

**Engagement:**
* In addition to retweeting described above, please perform a search on `#gitlab` and `gitlab` a few times per day (can be within Sprout or twitter) and favorite positive mentions
* Any particularly funny or complimentary posts, you can comment on with a gif, :blush:, “Thanks! We’re so glad you like it, let us know how we can help!” etc.
* Be creative but refer back to these guidelines for the [GitLab voice](/handbook/marketing/social-media-guidelines/#gitlab-voice)

**Signing off**
* If you know there is going to be a long (1+ days) delay between when you need to sign off and when the next person takes over (i.e., for Summit travel), I recommend scheduling a tweet saying something like “The team is heading to beautiful Cape Town for our xth Summit! See you on the other side :airplane:” This helps prime people for the disruption of regular content.

## 🎟 Social Event Strategies
Coverage for events across GitLab brand social channels varies and is dependent on investment level, on-the-ground activations, and core messaging. Social coverage for an event is not guaranteed.

Sponsoring or creating an event is a way to extend GitLab to new audiences, so promoting these events to our existing audiences on brand social channels does not work in the same direction. In order for an event to be social-able, there needs to be something that social audiences can gain. In most cases, promoting events on organic brand social channels is a way to tell our audiences that we are out in the world, doing big things, and are taken seriously. This means that simply because GitLab is a sponsor of an event does not mean that we will promote the event on organic brand social channels. 

It's critical to fill out a **social-event-request** issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues) as soon as there is enough information available to request organic social coverage. 

Once the social event request issue template is filled out, a member of the social team will determine what level of social coverage, if any, is appropriate. Levels of coverage and what each includes are listed below. Please note, that the descriptors of each tier are suggestions, and a specific event may be identified for a different tier for a number of factors. While most events fall into similar categories, specific coverage may need to be unique to the event.

#### Social Event Tiers and What They May Include
| Tier | Example Event |Equivalent | What's Included |Action|Example |
| ------ | ------ | ------ |------ |------ |------ |
| `1` | Any Integrated Corporate Marketing Event|On-Site Support, Channel Takeovers |  Everything in Tiers 2 and 4. Includes Tier 3 if appropriate. On-Site/At Event Support from a member of the social team. Social Channel "takeovers", e.g. creative focused on the event. |Social Team: Use existing issue to plan. Requester: Be sure to link event campaign epic and other comms/PR issues. It's critical for social + PR to be linked for Tier 1 events. If creative is necessary, open a **design-request-general** issue. |Consider Commit, AWS re:Invent, and others |
| `2` | Any event with a GitLab speaker, presentation or interactive element - *must be more than booth or attendance only* |Broadcasted via Posts on GitLab Brand Channels (Pre, During, Post) | Everything in Tier 4; Includes Tier 3 if appropriate. Organic social posts including custom copy. May include: scheduled posts in advance of the event, live coverage during, and/or post-event wrap-ups. | Social Team: Use existing issue to plan posts. Requester: You may need to include more info. If creative is necessary, open a **design-request-general** issue. |[Ex 1](https://twitter.com/gitlab/status/1228384465891790848?s=20) |
| `3` | Team Member Enablement for any event, when the event team makes this request |Advocacy, Team Members Sharing on Their Channels | Everything in Tier 4; Social team aiding to create copy for team members to share on their own social channels. *This tier is optional, only if the requester wants assistance with advocacy.* | Social Team: Open a **social-team-advocacy** issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues). If creative is necessary, open a **design-request-general** issue.|[Ex 1](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/1437) | 
| `4` | Booth or Attendance Only |Engagement Only | Ad-hoc retweets/engagement with social posts from other users and brands. No outbound broadcasts from brand social channels. The majority of events will fall under Tier 4. |Social Team: Set calendar reminders for event day(s) and open specific columns in Tweetdeck to support. Alert the advocates of live coverage as this impacts Zendesk activity. |[Ex 1](https://twitter.com/gitlab/status/1228340156769411072?s=20)|

#### Field Events + Social Media
Field events present a major challenge and a major opportunity across social channels. While we can geo-target our organic (existing followers) audience on some channels, the number of people represented in this audience may not warrant a social post on GitLab brand social channels.

The single best way to secure social support for field events is to request paid social advertising with the Digital Marketing Programs team. This allows for appropriate geo-targeting on any social channel. This does not need to be a major investment.

You can open a paid social advertising request by [opening a new issue in the Digital Marketing Programs Project](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new#) and choosing the **mktg-promotion-template**. The core social team is not the DRI for paid social, however, we can assist you with questions or details along the way.

#### Booth Location-Only Requests for Social Media Coverage
If your only request is 
> "We have a booth at this event and we want people to know where to find us."

Unless the event in question is in an integrated corporate marketing campaign, there are only two appropriate ways to tell social audiences that GitLab has a booth. 
1.  **GitLab Team Members use their personal/professional social channels to share with their audiences.** This could be considered a `Tier 3` or `Tier 4` event, where the social team can aid the event team, however, an issue is required in advance of the event. 
1. **Paid social advertising presents the necessary opportunity** - geo-targeting users in a defined region to tell them where GitLab has a booth.

This means that booth location-only requests are not fulfillable with global organic social media posts.

### FAQ on Social Event Coverage
##### I don't understand why we can't get the support we've asked for.
Organic social media has a number of priorities to mix for success. First, our global audiences need to gain value from our posts or we need to extend GitLab's brand somehow. Every event isn't relevant from a global scale, and in most cases, doesn't provide a brand value. Furthermore, agreeing to publish posts that would not perform well against our performance indicators is a self-sabotage for the social team. It's not in our best interest, as a team or as a company, to publish posts that won't perform well or that do not extend the GitLab brand.

##### I'm worried that my event won't get enough attention on social media.
Organic brand social channels have to maximize fit, content, and distribution in order to meet performance metrics. This yields more attention to the right content from the right kind of audience. An integrated plan for event coverage should never rely solely on social media. If you are deeply concerned about marketing your event, please consider an alternative to organic social, including paid social advertising.

##### When should I ask for paid social advertising for an event?
Generally speaking, there are two points to hit for paid social advertising to work. If the event **has a GitLab-owned landing page** and **needs to be geo-targeted to the right regions** then your event may be a good candidate for paid social advertising. *E.g. Our roadshow is coming to Chicago and we have a landing page for registrations.*

You can open a paid social advertising request by [opening a new issue in the Digital Marketing Programs Project](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new#) and choosing the **mktg-promotion-template**. The core social team is not the DRI for paid social, however, we can assist you with questions or details along the way.

##### Help! My event is in *insert short period of time*. Can the social team post?
If your event is in an appropriate tier for broadcasted posts on GitLab brand social channels, the social team will work with you to get these posts scheduled. Please keep in mind that the shorter the amount of time to prepare, the less likely there will be a comprehensive plan including the best copy, creative, and other elements. It's still best to get request issues open as soon as possible.

If last-minute requests become a frequent occurrence or there are other specifics preventing our cooperation, the social team reserves the right to decline the request.


## Giveaways <a name="giveaways"></a>

We use giveaways to encourage and thank our community for participating in marketing events such as surveys, user-generate-content campaigns, social programs, and more.

### Giveaways Process <a name="giveawaysprocess"></a>

**Pre-giveaway**
1. Create an issue and tag the Social Marketing Manager to determine the
rules of engagement and the Corporate Events Manager for prizes.
2. Create and publish an [Official Sweepstakes Rules page](#officialrules)

**Post-giveaway**
1. Winners must sign an Affidavit of Eligibility & Liability, Indemnity, and Publicity Release. Use the "Affidavit of Eligibility - Sweepstakes" template found on the google drive.
2. Announce the winners

### Social Support & Logistics for Giveaways

#### Creating the Campaign

- Set a launch date
- Ask for social image(s) with text (if organic posts only) explaining the offer/ask
- Set an initial deadline for submissions, so you can have multiple pushes at interval & ramp up energy  
- Finalize the delivery method: form vs. tweets vs. retweets, depending on the goals of the campaign
    - Pros of a form: Neat, uniform, easy for us to keep track of, no downsides of low engagement (i.e., responses not visible)
    - Pros of asking for submissions via Twitter: we could more easily RT cool responses, get more out of a hashtag, etc.
    - Pros of asking for RTs in exchange for swag: very little backend to do on social afterwards, except to announce the winners of swag
- Finalize the ask, making sure it's extremely clear what you want to happen (`Share your GitLab story!` `Tell us your favorite thing you made with GitLab` `tell us a time GitLab helped you out of a tight spot`)
    - Make sure the ask can be intuitively communicated via whichever delivery method you're using, i.e., the tweet doesn't need to explain everything if you're pointing to a form or blog post. If you're not pointing to anything, make sure the tweet plus possible image text must make sense by themselves. Use threads for more space!

#### Pre-launch

- Finalize the timeline for when the reminders/follow-ups will go out, add to social schedule and leave some space around them to RT/engage with responses
- Finalize copy for all pushes
- If swag is involved, create a google sheet with swag codes from the Event Marketing Manager
- Finalize hashtag
- Ask community advocates to review all copy (tweets, form, blog post) and adjust according to their suggestions
- Make sure the community advocates are aware of the campaign timeline/day-of
- Designate a social point person to be "on duty" for the day-of and one person who can serve as backup
- Let the broader GitLab team know that the social campaign is upcoming and ask for their support

#### Day of giveaway
- If you have entries for the giveaway in a spreadsheet, use [random.org](https://www.random.org/) to generate a random number. Match the number to the corresponding row in your spreadsheet to identify the winner. **Never enter email addresses or personal information of participants into a third-party site or system we do not control.**
- Try to schedule first push or ask a team member to tweet the first announcement early (ex: around 4 am PT) to try to have some overlap with all our timezones
- If you're asking for RTs in exchange for swag, make sure there's a clearly communicated cut-off to indicate that the giveaway will not stretch into perpetuity. One day-long is probably the longest you want a giveaway to stretch, or you can limit to number of items.
- Plan to engage live with people
   - If your promise was to give away one hoodie per 25 RTs, do it promptly after that milestone is crossed. It adds to the excitement and will get more people involved
- Announce each giveaway and use handles whenever possible, tell them to check their DMs
- DM the swag codes or whatever the item is
- In your copy, directly address the person/people like you are chatting with them irl
- RT and use gifs with abandon but also judgment

#### After the Giveaway
- Thank everyone promptly, internal & external
- Write in the logistics issue of any snags that came up or anything that could've gone better
- Amend hb as necessary for next time

### How to Create an Official Sweepstakes Rules Page <a name="officialrules"></a>

1. Create a new folder in `/source/sweepstakes/` in the www-gitlab-com project. Name the folder the same as the giveaway `/source/sweepstakes/name-of-giveaway`
2. Add an index.html.md file to the `/name-of-giveaway/` folder
3. Add the content of [this template](https://gitlab.com/gitlab-com/marketing/general/blob/0252a95b6b3b5cd87d537dabf3d1675023f1d07d/sweepstakes-official-rules-template.md) to the `index.html.md` file.
4. Replace all bold text with relevant information.
5. Create merge request and publish.

## Social Channels and Audience Segmentation <a name="social channels and audience segmentation"></a>

You can find a list of evergreen content assets their primary channel for promotion [here](https://docs.google.com/spreadsheets/d/1_RdHsIflAdLjvZKzCzMCa-xBgwZYoZgGBJr0E-6Djcs/edit?usp=sharing).

### Twitter <a name="twitter"></a>

**Content/Execution**
* Broad audience - all our content gets shared here, but tone should still appeal to devs.
* 5 or more tweets per day, plus retweets
  * Schedule between 12 am - 5 pm PT (A large part of our audience is in EMEA, and we find an increase in organic impressions and engagement when we schedule tweets for their morning and workday)
  * At least 30 mins apart
* Aim for variety & roughly even mix of main topics: Git, CI/CD, collaboration, DevOps, employer branding, product-specific, community articles & appreciation
* Check mentions at least once per day, pull out articles to share with the team and favorite positive mentions.
*  Keep a list of our most engaged followers; keep these in mind for opportunities outside of social--blogging, live streamed interview etc.

**Goals**
* 15 tweets per day
* 2-4 videos published natively per month
* 5 lead-generating content items from the backlog published per month
* Consider [takeovers.](http://www.telegraph.co.uk/news/2017/07/12/work-experience-boy-takes-southern-rails-twitter-account-things/) Would need to develop an instructions kit & think about how/who to pilot.

### Facebook

**Content/Execution**
- Developer/community- and thought-leadership focused
- 1-2 facebook posts per day
- At least 2 hours apart
- Live events are more company/culture focused
    - talks by or AMAs with People Ops, etc.

**Goals**
- Consider streaming a webcast on Facebook Live (a simple toggle switch in Zoom allows this) and compare the performance to YouTube live streaming
- When the speaker cameras are enabled in Zoom webinars, natively publish clips of webcasts for post-promotion.

### LinkedIn <a name="linkedin"></a>
Sometimes, posts that GitLab team-members propose for our blog may be a better fit for native publishing on LinkedIn. This is not a negative, it's usually due to the content team's strategic priorities at the time. The Managing Editor and the Social Marketing Manager will recommend that you publish on your own LinkedIn, and if you agree, the Social Marketing Manager will help you finalize the post and socialize it internally for best results. 

**Content/Execution**
- IT buyer/manager focus
- 3-5 posts per week
  - Non-tutorial original blog posts
  - Press releases/product announcements
- Posts around particular features, releases, company culture, people ops new practices, problem/solution or personal growth structured posts; include links back to homepage

**Goals**
* Identify key LinkedIn groups to join/share content in
* Native posts on LinkedIn
   * Encourage targeted employees to do so; think of incentive/framing as professional development that we will help edit.
* Curate and share articles from other publishers on remote work/culture that we want to elevate

### Medium <a name="medium"></a>
GitLab has a [Medium publication](https://medium.com/gitlab-magazine), and all GitLab team-members may be added as writers! To be added as a writer to the publication, [import](https://help.medium.com/hc/en-us/articles/214550207-Import-post) a blog post that you authored on about.gitlab.com/blog to your personal Medium account, and submit it to the GitLab publication (by hitting `edit` -> `submit to publication` -> `GitLab Magazine`). the Social Marketing Manager will approve you as a writer and help finalize the post before publishing. 

If you submit original content (i.e., not originally published somewhere else) to the publication for review, she may edit and publish your post. We want to highlight writers wherever possible, so we highly encourage you to import posts to your personal Medium.

**Content/Execution**
* Brand and thought leadership-focused
* Favorite articles about GitLab
* Aim to post one article to Medium every two weeks
* Cross-post our original thought-leadership posts to Medium, linking back to blog
  - Use the [import tool](https://help.medium.com/hc/en-us/articles/214550207-Import-post); do not copy and paste.

### YouTube <a name="youtube"></a>

**Content/Execution**
- Developer/community-focused
- Live events are more tech focused
  - Group Conversations, pick your brain meetings, demos, brainstorms, kickoffs

## Defining Social Media Sharing Information for web pages

**Per [Twitter's functionality](https://developer.twitter.com/en/docs/tweets/optimize-with-cards/guides/getting-started), we're moving to using custom OG tags across all owned pages, as well as, eliminating the redundant tags for Twitter cards.\
In our iterim process, please add opengraph images to `'/images/opengraph/page-name.png'`**

Social Media Sharing info is set by the post or page frontmatter, by adding two variables:

```yaml
description: "short description of the post or page"
twitter_image: '/images/tweets/image.png'

## Ensuring your Post Will Have a Functional Card and Image

When you post a link on Facebook or Twitter, either you can see only a link, or a full interactive card, which displays information about that link: title, **description**, **image** and URL.

For Facebook these cards are configured via [OpenGraph Meta Tags][OG]. Twitter Cards were recently set up for our website as well.

Please compare the following images illustrating post's tweets.

A complete card will look like this:

![Twitter Card example - complete][twitter-card-comp]

An incomplete card will look like this:

![Twitter Card example - incomplete][twitter-card-incomp]

Note that the [first post] has a **specific description** and the image is a **screenshot** of the post's cover image, taken from the [Blog landing page][blog]. This screenshot can be taken locally when previewing the site at `localhost:4567/blog/`.

```

This information is valid for the entire website, including all the webpages for about.GitLab.com, handbook, and blog posts.

### Images

All the images or screenshots for `twitter_image` should be pushed to the [www-gitlab-com] project at `/source/images/tweets/` and must be named after the page's file name.

For the second post above, note that the tweet image is the blog post cover image itself, not the screenshot. Also, there's no `description` provided in the frontmatter, so our Twitter Cards and Facebook's post will present the _fall back description_, which is the same for all about.GitLab.com.

For the handbook, make sure to name it so that it's obvious to which handbook it refers. For example, for the Marketing Handbook, the image file name is `handbook-marketing.png`. For the Team Handbook, the image is called `handbook-gitlab.png`. For Support, it would be named `handbook-support.png`, and so on.

### UTMs for tracking URLs

UTMs are used to track traffic sources & reach of posts/links. All external posts should contain a UTM parameter, please see [details in the Digital Marketing handbook](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#url-tagging).  

If you have questions or are unsure how to tag a URL please reach out to the Digital Marketing team &/or the Social Media Manager responsible for the campaign. 

### Description

The `description` meta tag [is important][description-tag]
for SEO, also is part of [Facebook Sharing][og] and [Twitter Cards]. We set it up in the
[post or page frontmatter](/handbook/marketing/blog/#frontmatter), as a small summary of what the post is about.

The description is not meant to repeat the post or page title, use your creativity to describe the content of the post or page.
Try to use about 70 to 100 chars in one sentence.

As soon as you add both description and social sharing image to a page or post, you must check and preview them with the [Twitter Card Validator]. You can also verify how it looks on the FB feed with the [Facebook Debugger].

### Examples

To see it working, you can either share the page on Twitter or Facebook, or just test it with the [Twitter Card Validator].

- Complete post, with `description` and `twitter_image` defined:
[GitLab Master Plan](/blog/2016/09/13/gitlab-master-plan/)
- Incomplete post, with only the `description` defined:
[Y Combinator Post](/blog/2016/09/30/gitlabs-application-for-y-combinator-winter-2015/)
- Incomplete post, with none defined: [8.9 Release](/blog/2016/06/22/gitlab-8-9-released/)
- Page with both defined: [Marketing Handbook](/handbook/marketing/)
- Page with only `twitter_image` defined: [Team Handbook](/handbook/)
- Page with none defined: [Blog landing page](/blog/)


<!-- Social Marketing Handbook - ref https://gitlab.com/gitlab-com/marketing/issues/524 -->
<!-- identifiers, in alphabetical order -->

[blog]: /blog/
[description-tag]: http://www.wordstream.com/meta-tags
[facebook debugger]: https://developers.facebook.com/tools/debug/
[first post]: /2016/07/19/markdown-kramdown-tips-and-tricks/
[OG]: https://developers.facebook.com/docs/sharing/webmasters#markup
[second post]: /2016/07/20/gitlab-is-open-core-github-is-closed-source/
[twitter card validator]: https://cards-dev.twitter.com/validator
[twitter cards]: https://dev.twitter.com/cards/overview
[twitter-card-comp]: /images/handbook/marketing/twitter-card-complete.jpg
[twitter-card-incomp]: /images/handbook/marketing/twitter-card-incomplete.jpg
[www-gitlab-com]: https://gitlab.com/gitlab-com/www-gitlab-com
