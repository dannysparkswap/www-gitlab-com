---
layout: handbook-page-toc
title: "Professional Services"
---
# Professional Services Handbook
{:.no_toc}

The Professional Services team at GitLab is a part of the [Customer Success](/handbook/customer-success) department. Professional Services team members ensure solution success by accelerating time to value to drive adoption and support product revenue growth. We do this through deivering product services, operational consulting services, and strategy consulting services.

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Mission
Accelerate our customers' DevOps transformation by delivering successful customer outcomes with tangible business value

## Goals

* Deliver services to improve operational efficiency, accelerate time to market and reduce risk through GitLab product adoption
* Provide consistency and quality through a repeatable methodology with increased focus on business value 

### Why GitLab Professional Services?

We have chosen to have a Professional Services offering at GitLab for several reasons, including benefits to both GitLab and our customers.

#### Benefits to our customers

Having a Professional Services team benefits our customers in the following ways:

* Faster value realization
* Improved customer experience
* Access to GitLab experts / best practices

#### Benefits to GitLab

Having a Professional Services team benefits GitLab and our [business model](/monetizing-and-being-open-source/) and [strategy](/company/strategy/) in the following ways:

* Improved retention and expansion
* Customer insights and feedback
* Experience and offers for partners

### How we deliver

We deliver on our Mission and Goals through two methods:

1. Direct to customers delivered via [GitLab team-members](/company/team/?department=professional-services) as described in our [professional services offerings](#professional-services-offerings)
1. Partner delivered to allow us to leverage partners in order to: 
  - Ensure we have local coverage globally
  - Complete services offerings
  - Create a partner revenue stream
  - Partner delivered services
  - Contracted delivery resources
  - Allow us to deliver and develop IP


## Roles and Responsibilities

| Name | Role | 
| ------------- |-------------|
| Arani Bhavadeep | [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Robert Clark | Public Sector Manager, Customer Success |
| Syeda Firdaus | [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Adriano Fonseca | [Sr. Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Michael Leopard |  [Federal Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Mike Lindsey | [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Michael Lutz |  [Sr. Director, Global Professional Services](https://about.gitlab.com/job-families/sales/director-of-professional-services/) | 
| Kendra Marquard | [Sr. Technical Instructional Designer, Customer Education](https://about.gitlab.com/job-families/sales/technical-instructional-designer-customer-education/) |
| Jo Marquez | [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Glenn Miller |  [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Jordan Ng | [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
 Dan Peric | [Sr. Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
 | Nicki Peric |  [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
 | Petar Prokic |  [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
 | Ed Slatt |  [Sr. Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| David Tacheny |  Manager, Professional Services |  
| Chris Timberlake |  [Sr. Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Laci Videmsky |  [Sr. Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Kevin Vogt |  [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Allison Walker | [Sr. Project Manager (PMO)](https://about.gitlab.com/job-families/sales/professional-services-project-manager/) |
| Christine Yoshida |  [Practice Manager, Education Services](https://about.gitlab.com/job-families/sales/professional-services-practice-manager/) |

## Metrics

### Long Term Profitability Targets
The long term gross margin target for Professional Services is 35%.

### Services Attach Rate for Large Accounts
The percent of Large Parent Accounts that have Professional Services associated with it or any linked SFDC Accounts. GitLab's target is above 80%. 
<embed width="100%" height="100%" src="<%= signed_periscope_url(chart: 6202331, dashboard: 446004, embed: 'v2') %>">

## How to order Professional Services

There are two methods for ordering professional services depending on whether the service is standard or custom. 
* Standard Offerings (non-customized): Sold using [Professional Services SKUs](#professional-services-skus) with pre-defined statement of work (SOW) documents. These offerings do not require custom scoping and are orderable directly from Zoura in SFDC.
* Custom Offerings: Sold using a custom SOW scoped using the [Services Calculator](http://services-calculator.gitlab.io/). 

**Process**

```mermaid
graph LR;
  a["Standard offering?"]-- Yes ---b["Add SKU to quote"];
  a-- No ---c["SA starts SOW w/Calculator"];
  c-->d["SA works w/PSE to finalize SOW"];
  z["Send customer proposal"];
  d-->z;
  b-->z;
```

For more details on selling professional services, see [Selling Professional Services](/handbook/customer-success/professional-services-engineering/selling).

For information on how to position professional services to your customer, see [Positioning Professional Services](/handbook/customer-success/professional-services-engineering/positioning).

### Lead time for starting

Often we are asked "what is your lead time for getting started with a project" or a customer may have a specific timeframe in which they want a project delivered.

As there may be many projects and proposals in flight at any time, there are a few rules we use when prioritizing and scheduling engagements:

* No engagement schedule can be committed to until a signed SOW is received.  
* While we can make a good faith effort to try and match a schedule ahead of that time, to be fair to customers who have committed to a services engagement we must prioritize those first. 
* Our typical lead time for starting an engagement is 4 weeks or less, but this can vary greatly as we scale the team and engagements.  
* Please check with the **Professional Services Manager** for the latest lead time details in the #[professional-services] Slack channel if you need a more accurate estimate.
* If your customer has a specific need, please discuss with the Professional Services Manager - either in the issue for the SOW or in #[professional-services] - before committing to anything for customer

### Custom services SOW creation and approval 

When scoping custom SOWs, the Professional Services (PS) team partners with the Solutions Architect (SA) to create a custom engagement that will help meet the customer's needs. Custom offerings must be reviewed and approved by the Director of Professional Services. 

#### SOW creation

The creation of new SOWs for customer proposals is owned by the [Solutions Architects](/handbook/customer-success/solutions-architects/).

To create a custom services SOW, start with the [Services Calculator](https://services-calculator.gitlab.io) which will open a new SOW issue on the [SOW Proposal Approval board](https://gitlab.com/groups/gitlab-com/customer-success/professional-services-group/-/boards/1353982?label_name[]=Services%20Calculator). Follow the specific [SOW process](/handbook/customer-success/professional-services-engineering/selling/#selling-services-workflow) for a complete explanation. 

We prefer customers to mark up our agreement and SOW document if they request changes. If they require the use of their own services terms or SOW, please contact the PS team.

#### SOW Proposal Approval Board

![SOW proposal board screenshot](/handbook/customer-success/professional-services-engineering/sow-approvals-board.png)

The [SOW Proposal Approval Board](https://gitlab.com/groups/gitlab-com/customer-success/professional-services-group/-/boards/1353982?label_name[]=Services%20Calculator) is used to approve all SOWs before sending them to a customer. 

The labels are (from left to right):

* `Open`: This issue has been created by the Services Calculator and waiting for the SA to confirm
* `proposal::Scoping`: Gathering information from the account team and customer required to scope the engagement
* `proposal::Writing`: The PS team is finalizing the terms of the SOW
* `proposal::Cost Estimate`: The Manager, Professional Services is preparing a [cost estimate](/handbook/customer-success/vision/#professional-services-standard-cost) using the [SOW Cost Estimate Calculator](https://docs.google.com/spreadsheets/d/16KFNRFe4E_oaqU7_ZGivoO7eU3-65dkMgVvK5Jvb7ZQ/edit#gid=158441360) for margin calculation purposes
* `proposal::Ready For Approval`: The PS team has completed the SOW, and it is ready to be approved by the Director of Professional Services.
* `proposal::Approved`: The SOW is ready to be sent  to the customer by the account team.  Once sent, this issue can be closed

**NOTE:** Issues that have not been updated by an SA in the last 30 days will be automatically closed.

## Services Delivery

### Implementation Plan

Each services engagement will include an Implementation Plan compiled based on the Statement of Work and other discussions the PS team has with the customer.

Each services engagement will have a Google Sheet that shows the contracted revenue and the estimated cost measured in hours or days of effort required to complete the engagement.  The cost estimate must be completed prior to SoW signature and attached to the opportunity in SFDC.

### Professional Services Workflows

For details on the specific Professional Services plays, see [Professional Services workflows](/handbook/customer-success/professional-services-engineering/workflows).

## Professional Services Offerings

### Hierarchy

1. **Levels**: There are three levels of service that represent the different services buyer personas.
1. **Categories**: Each level has one or many categories of service that define a set of related services either by type or product area.
1. **Offering**: Each category has one or many offerings that is a single consumable unit that has all of the required pieces to make a complete service offering.
1. **Offering Variants**: Each offering may have one or many variants that allow it to be deployed in different ways.  

Each offering at least has a variant called “standard” or “custom” to define if it can be delivered with a standard SKU / out of the box SOW. For example, enterprise versus commercial or remote versus on-site or one-time versus with an embedded engineer.

### Levels of Service

There are three levels of service we talk about when it comes to classifying our service offerings.

#### Product Services

**Installing, scaling & using the tool(s)**

Product services comprise services geared toward GitLab the software itself.  Getting a customer up and running with GitLab in a secure, highly-available environment to ensure their success with the application.

For example:

* Implementation & Integrations
* Training & Certification
* Performance tuning & advanced administration

See [Professional Services Offerings](/handbook/customer-success/professional-services-engineering/offerings) for a detailed listing of offerings.

**Planned Offering Maturity**

![product services slide 1](/handbook/customer-success/professional-services-engineering/product_services1.jpg)

![product services slide 2](/handbook/customer-success/professional-services-engineering/product_services2.jpg)

**Note:** Some offerings here represent future plans.  They will be turned into dynamic items using `services.yml` to remove the need for a screenshot.

#### Operational Consulting Services

**Processes aligned with value-added changes**

Operational Consulting Services help teams on the path towards full DevOps maturity adopt industry best-practices and advanced use cases of GitLab to accelerate their time to market.  These services live one level above Product Services in the sense that they are less focused on the tool.  Operational Consulting Services are more focused on the processes in place that helps make the tool a success across a large enterprise.

For example:
* Development & DevOps process consulting
* Increasing DevOps maturity
* Establishing & measuring cycle time

See [professional services offerings](/handbook/customer-success/professional-services-engineering/offerings) for a detailed listing of offerings.

**Planned offering maturity**

![operational consulting services slide 1](/handbook/customer-success/professional-services-engineering/operational_services1.jpg)

![operational consulting services slide 2](/handbook/customer-success/professional-services-engineering/operational_services2.jpg)

Note - some offerings here represent future plans.  They will be turned into dynamic items using `services.yml` to remove the need for a screenshot.

#### Strategic Consulting Services

**Adapt people’s way of thinking to the new paradigm**

Strategic Consulting Services live one level above Operational Consulting Services as they focus on organization changes and changes to the people and their behavior.  These changes are required to get the full value from a DevOps transformation.  We will provide some services in this area though the complete offering will be provided in collaboration with our partner ecosystem.

For example:

* Cultural transformation
* Executive coaching
* Organizational structure consulting
* Digital strategy

See [professional services offerings](/handbook/customer-success/professional-services-engineering/offerings) for a detailed listing of offerings.

**Planned offering maturity**

![strategic consulting services slide 1](/handbook/customer-success/professional-services-engineering/strategic_services.jpg)

Note - some offerings here represent future plans.  They will be turned into dynamic items using `services.yml` to remove the need for a screenshot.

### Offering maturity model

The services maturity framework provides for 5 maturity levels for offerings: planned, minimal, viable, complete and lovable.

* **Planned**: A future planned offering
* **Minimal**: The offering is defined, a vision for moving to complete exists 
* **Viable**: We have delivered the offering at least once, feeding lessons learned into completion plan. At least some marketing materials and execution plans from Complete
* **Complete**: An offering that can be consistently delivered: predictability in timing, results, and margin. See [full definition of complete](#complete-offering) below.
* **Lovable**: The offering is at full maturity, positive NPS & impact on customer’s adoption of GitLab product

#### Complete offering

Required items for complete maturity:

* Marketing page on /services
* Collateral for Sales org
  - Description of Services
  - Slides
  - Training: personas, learning goals
* Pricing
* Sales Enablement Training
  - e.g. a video or other sales enablement for SAs & SALs
* Clear definition of the breadth & depth of our offering
* Included in [Services Calculator](https://services-calculator.gitlab.io/)
* If applicable, **Standard variant** with
  - Standard SOW
  - Finalized SKU for use by Sales
* Custom SOW template
* Clear goals and objectives for offering
  - Education: Learning objectives
  - Other types: Measuring outcomes
* Engagement Execution Plan
  - Expectations, timing, process

### Professional Services SKUs

Some services, such as training, quick-start implementation, and other out-of-the-box implementation packages can be sold as a [SKU](#professional-services-skus) which has a fixed price and fixed SOW associated.

#### Current SKUs

Currently, the following services can be sold as a SKU standard offering.  These offerings can be added to any quote, and reference a standard SOW. There is no need for the customer to sign a custom SOW in order to purchase these services. These offerings can be delivered remotely or onsite with the customer.  One class SKU is considered a "session".  The sessions will generally be a minimum of 6 including breaks, Question & Answers, and Labs.  In the case of onsite delivery, travel and expenses (T&E) is passed on to the customer based on our [Professional Services terms](/terms/#consultancy) and [sponsored travel policy](/handbook/travel/sponsored/)

<%= partial("skus_table") %>

#### Creating a new Professional Services SKU

SKUs for new PS offerings are requested by the Professional Services Practice Manager. To request a new SKU, the Practice Manager must complete and determine the following items to gain approval.

* A specimen SOW that can be referenced by the Item
* Established cost-basis and assumptions
* Hours and hourly cost
* Delivery cost (hours * hourly cost)
* Included T&E assumption (if any)

The Practice Manager follows these steps to request creation of a new SKU.

1. Create an [issue in the Finance issue tracker](https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=New_PS_SKU) referencing the above requirements
1. Review with the Finance Business Partner for Sales
1. Make any require changes
1. Submit to VP of Customer Success for approval
1. Submit to CFO, CRO and CEO for approval
1. Once approved, submit to Accounting to create SKU in Zoura (note: Accounting does not create a SKU number but does add the offering to the product catalog)
1. Once it is added to the product catalog, make up a new SKU number for the offering and add it to the `Current SKUs` listing above by editing the services.yaml file and creating a merge request

## Professional Services team engagement and communication

### Contacting Professional Services

At GitLab, Professional Services is part of the [Customer Success department](/handbook/customer-success).  As such, you can engage with Professional Services by following the guidelines for [engaging with any Solutions Architect](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect).  This process ensures that the Customer Success department as a whole can understand the inbound needs of the account executive and our customers.

You can also reach the group via the #[professional-services] Slack Channel.

### Scheduling Professional Services

For scheduling specific customer engagements, we currently are slotting implementations while our group grows to support the demand for services.  If you have a concern about scheduling the engagement, this should be discussed at the Discovery phase.  If you want to check availability, ask in the #[professional-services] Slack Channel. In no case should you commit to dates before receipt of agreements, P.O., etc.

As a Professional Services Engineer, if an engagement has been approved, follow this process to get the engagement scheduled.

1. When a new issue comes in, create an event on the **"Professional Services Engagement"** calendar.
1. In the calendar event, invite the team email address.
1. Include a link to the GitLab.com issue in the Calendar event.
1. Change the calendar event coloring to be <span style="color:red">**red**</span>.  This will help show no resource has been assigned to it yet.
1. When a team member accepts the engagement, the team member should replace the team email address with their individual email on the invite list.
1. Add the customers email to the invite list.
1. Change the <span style="color:red">**red**</span> calendar coloring to <span style="color:green">**green**</span>.

It is recommended to Professional Services Engineers to integrate their PTO Ninja to **"Professional Services Engagement"** calendar because this improves the Resource Management View over the Engineers availability. You can set up this integration following the steps below: 

1. On PTO Ninja type `/ninja settings`;
1. From the Profile Settings dropbox select *Calendar*;
![Selecting Calendar](./pto-ninja-step-2.png)
1. On PTO Ninja Settings click in *Add* to add the calendar;
![Selecting Calendar](./pto-ninja-step-3.png)
1. Input the Calendar ID *gitlab.com_mi66cst9iudilf2b2vio0eu3b0@group.calendar.google.com*

On PTO Ninja it is possible to classify **ooo** (Out of Office) in the following categories:

- Vacation;
- Out Sick;
- Public Holliday;
- Bereavement;
- Parental Leave;
- Customer Visit;
- Conference;
- Mandatory Civilian Service.

**Obs:** Classifying the **ooo** accordingly, you won't be messing up with your vacation days counter.

If the engagement needs to be rescheduled, work with the team to find the next available time, and move the Calendar event to the newly selected time.

### Instructor communications with customers

Professional Services Engineers who will be delivering GitLab Education Services courses can use the following instructor planning and communication tools to ensure smooth interactions with customers:
- [email communication templates](https://docs.google.com/document/d/1rJ9q9gEzsumRxDhoWEe45u70efmKA0eWNg69WONuCYs/edit?usp=sharing)
- [Training Event Plan Template](https://docs.google.com/document/d/1huNauyfhFPvLCuo-9T7Ol3FtBDYowYxiP_T5ItP2FN4/edit?usp=sharing)

### Professional Services issue board

The [Professional Services Issue Board is available here](https://gitlab.com/gitlab-com/customer-success/professional-services-group/ps-plan/boards).  This board contains everything that the group is working on - from strategic initiatives to [SOW writing](#statement-of-work-creation), all team activity is available here.

Issues are created for all work by Professional Services.  

#### Epic categories 

- [Billable] - *These should have their own customer specific Epics and SOW issues*
- [NonBillable] - *Work that will never be part of a signed SOW*
  - [PTO] - *Vacation, Out sick, Public Holliday, etc.*
  - [Team Objectives] - *Projects, Planning, Team building, etc.*
  - [Meetings] - *Non customer related meetings*
  - [Personal Enablement] - *Training, Co-working sessions, etc.*
  - [GitLab Contributions] - *Contributions like comments in issues, code, documentation, code reviews, etc.*
  - [Generic Tasks] - *Other, mostly administrative, daily tasks*
- [PreSales] - *These should have their own customer specific Epics and SOW issues*

#### 3 types of project tracking labels

- **Packaged**: Complete or incomplete
- **Custom or standard SOW** (more than 4 working weeks): Tracks completed percentage (25%, 50%, 75% or 100%), unless a Milestone is specified in the SOW
- **Embedded Engineer** (time and materials project): Bill time and materials direct to customer

#### Time tracking

The Professional Services team will track all hours.  Detailed and accurate time tracking records of hours is essential to ensure revenue can be recognized based upon percentage completion of a project scope as outlined in a Statement of Work ("SOW"), and this data is used in the calculation of gross margin.

Billable hours represent work hours that a staff member reports as being aligned to a specific SOW. The format for daily time tracking for each team member is shown below, and should be reviewed by the professional services leadership for approval and sign-off before being submitted each Monday (for the week prior) to the Finance Business Partner for Sales. Rounding to the nearest hour is acceptable, and please ensure data is recorded in numeric format without the use of text strings such as "hrs".

| Manager Approved Y/N | Finance Accepted Y/N |
| ------------- |-------------|
| Name      | SOW 1        |  SOW 2 | SOW 3  | SOW 4 (etc...)  | Total Billable Hrs  |
| --------- |:------------:| ------:|-------:|----------------:|-----:               |
| PSE 1     |  hours spent | X.X    |X.X     |X.X              |X.X                  | 
| PSE 2     |  hours spent | X.X    |X.X     |X.X              |X.X                  |  
| PSE 3     |  hours spent | X.X    |X.X     |X.X              |X.X                  |

##### Time tracking workflow

- Professional Services Engineers are required to track **all hours** using issues and all hours for the week should add up to a minimum of 40, or whatever the hours worked per week is in your country.
- All Issues are attached to Epics using the [epic categories](#epic-categories) as a guide.
- PSE [Hours Report Generator] pulls all time spent from issues that the user is either an author of, or an assignee, and creates the time tracking spreadsheet
- A **PSE Manager** will Approve the hours, create an issue and attach it to the Time Tracking Epic with the *ManagerCertifiedTimesheet* label

#### Project completion

At the conclusion of the Statement of Work the Professional Services Engineer will prepare a cost vs actual analysis. This analysis will be filed in SFDC.  When filed in SFDC the Professional Services Engineer will **@mention** the Controller and Finance Business Partner, Sales in SFDC Chatter.

#### SOW issues

When requesting an SOW, Account Executives or Professional Services team members should use the SOW issue template, which automatically shows the required information as well as labels the issue appropriately.

#### Strategic initiatives

Strategic initiatives are undertaken by the Professional Services team to leverage team member time in a given area of the Customer Success Department.  This can include increased documentation, better training resources or program development.

[Hours Report Generator]: https://gitlab-com.gitlab.io/customer-success/professional-services-group/issue-mover/
[Billable]: https://gitlab.com/groups/gitlab-com/customer-success/professional-services-group/-/epics/249
[Presales]: https://gitlab.com/groups/gitlab-com/customer-success/professional-services-group/-/epics/251
[NonBillable]: https://gitlab.com/groups/gitlab-com/customer-success/professional-services-group/-/epics/250
[Services Calculator]: https://gitlab.com/services-calculator/services-calculator.gitlab.io
[Hours Report]: https://gitlab-com.gitlab.io/customer-success/professional-services-group/issue-mover/
[Team Objectives]: https://gitlab.com/groups/gitlab-com/customer-success/professional-services-group/-/epics/72
[Personal Enablement]: https://gitlab.com/groups/gitlab-com/customer-success/professional-services-group/-/epics/145
[Meetings]: https://gitlab.com/groups/gitlab-com/customer-success/professional-services-group/-/epics/240
[PTO]: https://gitlab.com/groups/gitlab-com/customer-success/professional-services-group/-/epics/237
[Generic Tasks]: https://gitlab.com/groups/gitlab-com/customer-success/professional-services-group/-/epics/264
[GitLab Contributions]: https://gitlab.com/groups/gitlab-com/customer-success/professional-services-group/-/epics/255
[professional-services]: https://gitlab.slack.com/archives/CFRLYG77X
