---
layout: markdown_page
title: "Product Direction - Frontend Foundation"
---

- TOC
{:toc}

This is the product vision for Frontend Foundation, and it is currently a work in progress.

## Overview

The Frontend Foundation category is responsible for our design system [Pajamas](https://design.gitlab.com/) and our frontend architecture: webpack, [PWA](https://developers.google.com/web/progressive-web-apps) needs for improved mobile support, and more.

## Themes

Work in progress.
