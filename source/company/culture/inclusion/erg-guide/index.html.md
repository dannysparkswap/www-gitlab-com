---
layout: markdown_page
title: "ERG - Employee Resource Group Guide"
---

## On this page
{:.no_toc}

- TOC
{:toc}

##  Introduction

On this page you will have an overview of what is needed to start and sustain a GitLab ERG (Employee Resource Group)

