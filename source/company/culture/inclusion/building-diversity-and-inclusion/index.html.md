---
layout: markdown_page
title: "Building and Sustaining a Diverse and Inclusive Remote Culture"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we are detailing how to build and sustain a Diverse and Inclusive Culture in a remote environment. 

## Where do you start with building a Diverse and Inclusive remote environment?

There are many elements to consider when building a Diverse and Inclusive culture.  

## Elements to consider
1.  Defining Diversity and Inclusion - A great place to begin is to set a foundation of the basic understanding of how your company defines these terms.  Most places you will hear them used interchangeably.  Understanding they are different is essential to executing the body of work.  As an example, you can hire as many diverse candidates but if you don't create an inclusive environment the work can be in vain.  

2.  Evaluating the unique landscape of your company - What are you already doing in this space?  What is the current feedback?What are team members saying with company engagement surveys?  What are the goals you are wanting to achieve?  What are the metrics saying?

3.  Naming of D&I - Although Diversity & Inclusion are often understood globally there are other terms that can be leveraged to name your efforts.  The naming of this body of work should be unique to your company.  Examples could include (i.e, Belonging, Inclusion & Collaborations, etc.)

4.  D&I Strategy 

5.  Creating ERGs (Employee Resource Groups)?

6.  Creating a D&I Advisory Group?
7.  D&I Initiatives?
8.  D&I Awards for Recognition?
9.  D&I Surveys?
10.  D&I Strategy?
11.  D&I Framework?
12.  D&I Training for all levels?
13.  Inclusive Benefits?
14.  D&I Engagement?

