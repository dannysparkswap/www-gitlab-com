---
layout: handbook-page-toc
title: "GitLab Contribute Frequently Asked Questions"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Registration

#### I'm new/I'm a manager and have a new hire. Will I/my new hire be invited? When will invitations been sent? 

##### New team members who have signed their work agreement on or before March 6, 2020 - with a start date up to and no later than March 18, 2020 - will be eligible to attend Contribute 2020 so that travel and accommodation can be arranged no less than two weeks prior to the event. This has been decided by the E-Group.
 
Any new team members with start dates up to and including Feb. 24, 2020 will receive a Contribute registration email and TripActions invite sent to their GitLab email addresses on their first or second day of work, with 2 weeks to register and book their travel. These emails will be sent on a weekly basis until and including on Feb. 24, 2020.

For any new team members with start dates after Feb 24, 2020 and up to and including March 18, 2020, invitations will be sent daily to their personal email addresses with a 1-week deadline to register and book travel. For invitations sent to personal email addresses, only the Contribute registration email will be sent (no TripActions invitation). Those team members will be instructed how to book travel on their own, and submit reimbursement when they have access to Expensify on their first day; or they may send their preferred, in-budget itinerary to `contribute@domain.com` and we will book their travel for them.

  * Example 1: On Feb 10th, team member GK signs their contract and they have a start date of March 9th. Feb 11th, all email invitations are sent to new GitLab team members who are starting that week (Feb 10-14) to their GitLab email addresses. In addition, we will also send invitations to team members such as GK who have signed a contract now with start dates that are very close to Contribute. GK will receive only a registration email to their personal email. Both of these groups will still have 2 weeks to register and book travel as they will receive their invitations before Feb 24.

  * Example 2: On March 5, team member JW signs their contract with a start date of March 10. People Experience Associates require a minimum of [4 business days](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#timing) before the new hire's start date (based on the new hire’s time zone) to complete all onboarding tasks. Therefore, People Ops would state that they cannot onboard a new team member with that short of a time frame, as they need 4 full business days to prepare. They would require team member JW to start March 12 at the earliest. In terms of their invitations, signed contracts will be reviewed on a daily basis in March and team member JW will be sent their invitation March 6 with a 1-week deadline to register and book travel.

  * Example 3: On March 7, team member AD signs their contract with a start date of March 17. Since they are signing after the deadline of March 6 and even with a start date before Contribute, they will not be invited to Contribute.
  
#### How do I register?

You will receive an email from `contribute@gitlab.com` inviting you to register (please see [above](#im-newim-a-manager-and-have-a-new-hire-will-imy-new-hire-be-invited-when-will-invitations-been-sent) for details on when invitations will be sent to new hires). Please follow the link in the email to register.

**Booking your travel (through TripActions or otherwise) is not sufficient to register for Contribute. You need to follow in the link in your invitation email to register.** If you do not register, we cannot arrange accommodation, transfers, excursions, or any other elements of Contribute for you.

#### I am not sure I can attend just yet, what should I do? 
You may have 14 days or less from the time you receive your registration email to register for Contribute. Your exact deadline time frame is listed in your registration invitation email. If you'd like to request an extension, please email `contribute@gitlab.com` with information on why you'd like more time to decide.

#### How do I modify my registration? 
There is a link to `Manage Registration Details` in the confirmation email sent after you register. 

If you deleted/can't find your confirmation email, you can visit [https://events.bizzabo.com/gitlab-contribute/settings/ticket](https://events.bizzabo.com/gitlab-contribute/settings/ticket).

#### How do I deregister from Contribute?
We're sorry to hear you won't be able to join us. To deregister find the confirmation email sent after you registered, and click on `Manage Registration Details`, that will allow you to cancel your registration.

If you don't have access to that email, or if you need to deregister your SO as well, please email `contribute@gitlab.com` and the team will take care of it.

## Concerns about the Novel Coronavirus

The Contribute team is continually monitoring the Novel Coronavirus situation to ensure the safety of all team members and guests. We will be following all recommended guidelines from the Centers for Disease Control and Prevention (CDC) and the World Health Organization (WHO) as the situation progresses.

There are no plans to cancel Contribute. Note that the WHO is currently advising against the application of any restrictions on international traffic. Based on the information currently available on the outbreak, the CDC is recommending standard safety practices that should be instituted during any flu season, and there are no US Dept of State Travel Advisories for countries other than China with relation to Novel Coronavirus. 

We understand however that everyone has different circumstances and needs, and we encourage all team members to make the best decision for themselves with regard to traveling to Contribute. If you would like to discuss your specific situation, please reach out to `contribute@gitlab.com`.

For the safety of our event participants, we will institute numerous safety measures and guidelines to ensure everyone’s health and wellbeing including:

- Keep up-to-date information on our event websites pertaining to the status of the event, and precautions for attendees onsite
- Communicate pertinent updates to all registered attendees leading up to the event
- Request that anyone sick or experiencing any cold/flu-like symptoms stay home for their own safety and the safety of others
- We ask that any attendees onsite who might begin experience cold or flu-like symptoms (fever, cough, trouble breathing), to please seek medical care right away. We will have local medical care contact information available for all attendees and can assist any attendee with transportation to a local clinic.
- We also ask all registered attendees to self-certify that if they have traveled to China in the last month, they ensure that at least 14 days have passed from the time they arrived from China to the event and that they have shown no flu-like symptoms in that time. If they have, we ask that for their own safety and the safety of other attendees, they refrain from attending Contribute.

We will continue to regularly check the latest official information leading up to the event and encourage team members to do the same. We understand that there is a lot of fear and uncertainty right now as we are in the early stages of the outbreak and a lot is still unknown, however, we want to ensure we’re making decisions based on factual recommendations rather than rumor or fear. 

### Recommended resources

- [World Health Organization (WHO) Novel Coronavirus Daily Situation Reports](https://www.who.int/emergencies/diseases/novel-coronavirus-2019/situation-reports)
- [Centers for Disease Control and Prevention (CDC) Situation Summary](https://www.cdc.gov/coronavirus/2019-ncov/index.html)
- [Real-Time Data Dashboard from Johns Hopkins University](https://gisanddata.maps.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6)
- Please also check your local government travel advisories

## Booking travel

We’ve signed on with TripActions for all team travel. For Contribute you will receive a separate invitation email with the specific budget and travel dates for Prague directly from Trip Actions. However, since there may be cheaper flights outside of TripActions, please do a search on other websites, such as Google Flights, airline websites, and Skyscanner, before booking. If you find a cheaper flight elsewhere, please see [Booking travel outside TripActions](#booking-travel-outside-tripactions) below for more information.

Please use the invitation email link to book your flight in TripActions for correct finance tagging.

### Deadline to book travel 

Please see your registration email to determine your booking deadline.

### Budget

All amounts are listed in United States Dollars (USD). Convert the amount to your local currency if you don't use USD.
- $1200 for team members in the US/Canada.
- $200 for team members in EMEA
- $1300 for the team in APAC and other regions such as Central and South America or Africa.

Your email invite is linked to the correct budget for your location.

`**All travel budget allocated is not applicable for guest travel expenses.**`

The [handbook policy of $300 towards an upgrade on flights](/handbook/spending-company-money) of 9+ hours does not apply to Contribute*.

_*However, if your travel time is 24+ hours or includes 3+ connections please feel free to expense up to $300 towards an upgrade (the $300 is for the round trip, not each way), if you are traveling over 30 hours please let us know asap and we can arrange for you to arrive a day earlier.
If your height is greater than 1.95m or 6'5", please seek approval from your manager first if you would like to upgrade your flights, as the additional cost will come out of your team's budget._

Please see the [Expenses](#what-expenses-are-covered-for-team-members) section below for more on what expenses are covered for team members.

### Using TripActions

If you’re logged in, you can find the tab “Invitations” in the top menu where you can again find the Prague link to book. Please make sure to use this for correct finance tagging.

### Booking travel outside TripActions

If you've found a cheaper or more direct flight outside of TripActions, you can book in two ways:

Book on your personal credit card and submit for reimbursement. When self booking you can only expense up to the max in your region. Make sure to use the tag "Prague Contribute 2020" in the "classifications" field in Expensify.
You can contact TripActions directly, provide them with the flight information, and ask that they book the flight on the company card.

If you'd like to travel by train, you can use your local railway website to book your travel. Please feel free to reach out with any questions and note that the budget will be the same as stated above for EMEA.

For anyone making any sort of booking outside of Trip Actions, please make sure to email your full itinerary confirmation to contributetravel@gitlab.com as soon as you have booked it.

### Travel FAQs

#### What if I can't find a flight within the budget for my region? 
Please do searches outside of TripActions on Google Flights, airline websites, and other travel sites to see whether there's a price difference.

If you are still unable to find something that works, please reach out to `contributetravel@gitlab.com`, and we will do our best to accommodate you. When you email the Contribute team, please attach screenshots of your research on both TripActions and external sites. 

**Please note that the Contribute team must approve your overbudget flight BEFORE you book. Failure to do so may result in covering the overage out of pocket. Your manager will also be contacted in the event you book outside policy.**

#### What about travel to and from the airport?

Please see the [expenses section](#what-expenses-are-covered-for-team-members) below.

#### What if I don't have a passport/need to renew my passport? 
If you don't have a passport yet, please enter `000000000000` for now. Apply for a passport **ASAP**. When you have it come back to modify your registration and enter the number.

If you are renewing your passport, enter your current number and come back to modify it when you have the new passport.

Please see [Passport costs](#passport-costs) below for guidance on expensing your passport application fee.

#### I need an invitation letter from GitLab to apply for a visa. Where can I get one? 

Also, can I get one for my SO and/or children? 

Please check the [handbook page on visas](/handbook/people-group/visas/#prague-contribute-2020-visa-invitation-letter) for details about getting an invitation letter.

#### What if I want to book my travel to/from a city other than Prague? 
If you want to extend your trip to visit another city in the area, it's fine to book and expense your travel to/from another destination, provided it costs no more than traveling directly to/from Prague and falls within the budget for your region.

#### What if I want to extend my trip and travel on different days? 
If you want to extend your trip, it's fine to book and expense your travel on different days, provided it costs no more than traveling directly to/from Prague and falls within the budget for your region.

#### When should I arrive/depart? 
Plan to **arrive on March 22nd** (this means you may need to leave your home on the 21st). This is arrivals day and there are no planned activities for this day. Even if you arrive at 11:45pm you will not miss out on anything and the team will be there to check you in.

If you arrive early on the 22nd, you may not be able to check into your hotel room right away, but the team will be there to greet you, and the hotel can hold your luggage so you're free to explore the city.

**March 27th** is the departures day; you can leave at any time on that day as the event concludes the night before and no activities are planned for the 27th. We will have transport back to the hotel from the final party anytime from 9pm on the 26th, but if you want to enjoy the party for as long as possible (in the past it has ended around 1am), we suggest booking an afternoon or evening flight home.

#### I found cheaper flights. Can I expense, or do I have to use TripActions? 
If you find flights outside of TripActions that are cheaper, you can purchase and expense the flight, see [Booking travel outside TripActions](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/blob/master/booking-travel.md#booking-travel-outside-tripactions).

#### Can I use the $300 upgrade for my flight? 
The [handbook policy of $300 towards an upgrade on flights of 9+ hours](/handbook/spending-company-money) **does not apply to Contribute**.

However, if your travel time is 24+ hours or includes 3+ connections, please feel free to expense up to $300 towards an upgrade (i.e. $300 total, not per flight).

If you’re traveling 30+ hours, please let us know asap, and we can arrange for you to arrive a day earlier.

If you are taller than 1.95 m or 6'5", you can upgrade to Economy Plus. Please email `contributetravel@` before booking.

#### I need a Schengen visa for Contribute. What happens if my visa application is declined and I need to cancel my travel plans? 
First of all, we hope this doesn't happen to you! But if it did, any cancellation fees would be taken care of by GitLab.

This is also applicable if you are unable to apply for visa before the deadline for booking flights. You can book your tickets before the deadline if you are unable to apply for a visa at that time. That way, you get better rates on your tickets. We can cancel the tickets based on your status of Visa later, and GitLab will take care of any cancellation fees.

Please see [Visa application costs](#visa-application-costs) below for guidance on expensing.

#### I'd like to travel by train to Contribute but this won't be within budget for my region. What can I do? 
Thanks for being eco-conscious! Please email `contributetravel@gitlab.com` with the price difference and we will do our best to accomodate you. 

#### What if I need to come early for a team meeting?
Once your manager notifies the planning team at `contribute@gitlab.com` we'll make arrangements for lodging. 

## Guests

#### Can I bring a guest? 
Yes, GitLabbers may bring one (1) guest, and they're encouraged to buy a ticket to attend. This way they will be able to join in on all activities, meals, excursions, workshops, and more. Any guests that do not have a ticket will not be able to participate in any Contribute activity.

#### Can my guest/child attend without a ticket to Contribute? 
This is discouraged for a few reasons. The purpose of Contribute is to connect with our team face to face, as we only gather in person once every nine months. If your guest is unable to join in on any activities, you may find yourself torn between spending time with other GitLabbers and hanging out with your guest, which could mean that you miss out on bonding with teammates.

It is also not guaranteed that you will get a private room if you request one (see the [room info page](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/blob/master/room-info.md)). The safest way to being your SO to Contribute is to buy them a ticket. The cost of a private room to share with your guest is included in their ticket.

That said, we understand that Contribute is a long time away from your family, and if the only way to attend is to bring our child/guest/both, then we won't charge a guest ticket.

**Please note we cannot accommodate anyone under 18 at any Contribute activity, such as meals, excursions or workshops.**

#### How do I register my guest? 
If you are buying a ticket for your guest for Contribute, you will need to register them separately. 

#### When is the latest I can register my guest to attend? 
Your guest has the same deadline as you do. 

#### Is my guest's ticket refundable if they have do to cancel for some reason? 
It is refundable up to **45 days before** Contribute. 

## Accommodation

During registration, you can express your rooming preferences. Final room assignments will be shared at the beginning of March.

### Hotels

We will be staying at the Hilton Prague (Pobřežní 1, Prague, Czech Republic, 186 00) and Hilton Prague Old Town (V Celnici 7, Prague, Czech Republic, 110 00).
You will not necessarily be in the same hotel as the rest of your team.

The Hilton Prague is our primary hotel where workshops and meetings will take place.

As it stands, most team members will be in the Hilton Prague, with some larger rooms reserved for those traveling with family in the Hilton Prague Old Town.

### Sharing a room

There is no additional cost if you opt to share a room. 

- You can choose a roommate by putting their name down when you register (or go back and edit your registration details). We will pair roommates only if both people choose each other, so please check with someone before you name them!
- If you don't choose a roommate, one will be allocated to you based on the preferences you note during registration. 

We encourage you to schedule coffee chats with your roommate before Contribute.

### Private rooms

#### With your guest

If you buy a Contribute ticket for your guest, you will automatically get a private room to share with them. The cost of this is included in your guest's ticket.

If your guest is joining you, **it is strongly recommended that they have a ticket for Contribute**, rather than joining independently and sharing a single private
room with you. The intention of Contribute is to be together with our team for IRL networking, relationship building, and experiences both with team members as a whole, 
and specialized department activities throughout the day and some evenings. If a guest ticket is purchased, they are welcome to attend any and all elements 
of Contribute (workshops, food and beverage, events, excursions, lodging, etc.). If your guest doesn't have a ticket, they will not be able to join any
element of Contribute, which might be isolating for them and could lead to you feeling torn between participating with the team and spending time with your guest. 
**Please note that only registered guests can be accommodated on shuttles to/from the Prague airport. If you are bringing unregistered guests, you must arrange for your own transportation
at your own expense.**

#### On your own

If you want to have a private room to yourself or to share with your guest who doesn't have a Contribute ticket, there is an additional cost of US$500. 
This is a fixed rate for GitLab and will not change, regardless of fluctuations in the listed price on the hotel's own website.

There may not be sufficient private rooms to accommodate everyone
who wants one, so please note this is not guaranteed. It is best to register early as it is first come, first served. We will also take personal/medical 
needs into account if the demand for private rooms outstrips supply.

We will notify team members who have requested a private room closer to the event. (We expect this to be [around mid-February](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/issues/17#note_261359546).) We will also let you know when and how to pay the fee closer to Contribute.

#### When do I pay for a single room? 

Instructions for this will be provided when the rooming list goes out at the beginning of March.

### Other lodging options

Team members are not permitted to stay in other accommodations during Contribute, such as Airbnbs or other hotels due to security reasons. Furthermore, the goal of Contribute is to build bonds with the GitLab community, which is easier to accomplish when we're staying at the same locations.

### Looking to stay additional days at the Contribute hotel?

If you want some extra time to explore Prague, this year we have added an option to select additional night stays before or after Contribute 
in the registration process. This will be an out-of-pocket cost of 4400 CZK + VAT (approximately $241 USD; this includes breakfast but not taxes) per night, 
but we will confirm the registration on your behalf. You will not need to check back in or out, and payment will be due upon checkin.

If you find a better rate, feel free to book and pay for it independently, but note that you will need to check in and out separately for your additional days, and you may move rooms.

## Attending Contribute

#### Is it possible to attend Contribute for a shorter amount of time? 
While we encourage everyone to attend the entire event because the program is crafted to ebb and flow with various learnings and differentiated interactions - both departmental and cross functional - we understand that this is a long time away from family and other priorities, so we're able to accept partial bookings as long as team members can **attend for at least 2 full days excluding Arrivals Day**.

#### What if I have to leave early?

In the case of an emergency that requires you to leave Contribute, please reach out to an Ambassador. We'll help find transportation and provide any other support you need. You won't be expected to reimburse GitLab for anything.

## What to expect at Contribute

### How much free time will there be during Contribute? 
You can count on having free time during Arrivals Day and in the evenings. You’ll have a packed schedule the rest of the time, including excursions, workshops, and keynotes. Please note that events are not mandatory, so if you’re not up for an excursion or a workshop, you’re not obligated to attend. However, the goal of Contribute is to build bonds with each other and cultivate a strong community, so if you decide not to attend an event, we hope that you spend that time recharging or getting to know others.

### How can I sign up for excursions?

Registration for excursions opened at 5am PT on February 20. We sent a Google Form to all registered attendees; please check the email address you used to register for Contribute and let us know if you still haven't received an email. We understand this may not be a convenient time for all time zones – being a global company makes this a challenge. We have tried to offer a wide range of choices for all interests and levels of activity. You can view [descriptions of all excursions here](/events/gitlab-contribute/#excursions).

- There are two excursion slots: a half day on Tuesday afternoon, and a full day on Thursday. You will need to indicate your top two choices for both days, so that you have a backup choice in the event that your first choice is fully booked. 
- If you have one excursion you are most interested in joining, we suggest making it your top choice for both days. Don't worry, we won't allocate you to it on both days!
-  "Wake up Prague" is a bonus excursion taking place early on Tuesday morning, which you can join in addition to the Tuesday afternoon and Thursday full-day excursions. You don't need to sign up for this in the Google Form. We will ask everyone to indicate interest in this excursion closer to Contribute.

#### How does SO registration for excursions work?

If your SO has a ticket for Contribute, they should have received an email with the Google Form to fill out as well. If you and your SO both send your forms in around the same time, chances are high that you will be placed on the same excursions (assuming you gave the same first and second choices!). 

Unfortunately we can't accommodate team members registering both themselves and SOs for excursions with one Google Form, but we are noting guests with registered +1s to do everything possible to pair team members with their SOs. 

#### My SO doesn't have a Contribute ticket. Can I pay for them to join an excursion?

Unfortunately not, we have provided headcounts to the tour operators based on registered attendees only.

#### When will we hear which excursions we are joining?

We will let everyone know which excursions they are registered for in a few weeks. We promise you will have plenty of time to plan and pack accordingly!

#### What happens if the weather is bad?

If you chose an outdoor excursion, the tour operator will likely make reasonable adjustments for the excursion to go ahead. We will be accommodating of weather-related cancelations onsite,  because even though as a company we will still be paying, our team members' sense of safety is paramount.  

#### What if I need to cancel my place on an excursion?

We will not be charging team members for cancellations, however please keep "[spending company money like your own](/handbook/spending-company-money/#guidelines)" in mind and be aware that GitLab will pay for your place on an excursion whether you attend or not. That being said, we understand that things happen and don't want to force people to do something that should be enjoyable for fear of being fined. 

 **There will be an excursion desk onsite dedicated to assisting with any wait lists, changes, cancellations etc.**

### How do I sign up for workshops, panels, etc.?

We will be opening registration for all sessions soon, please watch this space! In the meantime you can view a [tentative schedule here](https://contribute2020prague.sched.com/).

### What about self-organized activities outside of the official schedule?

Please feel free to arrange and participate in these! You can find them on the [issue tracker using the 'self-organized activity' label](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/issues?label_name=self-organized+activity), and create a new issue using that label if you want to do something that's not listed. Some existing issues are:

1. [Football (soccer) game](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/issues/19)
1. [GitLab band](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/issues/12)
1. [Board Games @ Contribute 2020](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/issues/64)
1. [Climbing Gym outing](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/issues/22)
1. [outing - nerf arena](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/issues/33)

### What is Team Day? 
Wednesday, March 25 is Team Day. On this day we will have designated Team Time where functional groups and teams can organize an activity together, followed by a [team dinner](#what-should-managers-do-about-team-dinners) in the evening. 

It is best to organize Team Time with the group you work with day to day. This can be a meeting during the day with a stage (multiple teams) and then a dinner with a team (smaller group). Or, it could be a meeting with your team (e.g. Marketing Operations or Field Marketing) and a [dinner with a larger group](#what-should-managers-do-about-team-dinners) (all of Marketing). The EBA tam is responsible for organizing and relaying their teams requests. They will be in touch with directors and managers on how they will be doing so. 

**EBA's need to submit [this form](https://docs.google.com/forms/d/e/1FAIpQLSchAGTyJ2Rs4Kscc3tt0G-cieXJIixFFQ1DSp9ZlS26tNLwZQ/viewform) on AV/ meeting space requests. Please do not complete without consulting your manager/ director to make sure they don't have plans for your larger group.** 

#### What is the budget for Team Time?

* $50 per registered attendee for dinner (dinners will **not** be self organized. The contribute team will be organizing dinner. Anything organized outside the contribute team in regards to dinner will be applied to the team budget, and **NOT** the Contribute budget.) We will send a follow-up email about team dinners and reservations.
* An additional $50 per registered attendee outside of the dinner.
    * This could be for a team outing, for expenses related to meeting space that go beyond the basic room and AV setup, or for lunch if teams decide to not take the bag lunches.
    * Each team should decide how they are going to use this money, it is not for individual employee usage.
    * See [can we plan our own excursions](#can-we-plan-our-own-excursions) for more.
    * Any additional costs come from the team's budget.

#### What do we do for lunch during Team Time?

We will have regularly scheduled meals at the hotel. We can arrange for bag lunches for teams going offsite at no additional cost to the team. We can take into account any food allergies and dietary restrictions, but the menu for bag lunches will be predetermined in coordination with the hotel.

If teams want to have lunch outside of this, it comes out of the budgets as described above.

#### Where do we make requests for space during Team Time?
* Your team's EBA will likely already have had submitted this form, please check with them before submitting a duplicate. EBAs will submit all meeting space requests on [this form](https://docs.google.com/forms/d/e/1FAIpQLSchAGTyJ2Rs4Kscc3tt0G-cieXJIixFFQ1DSp9ZlS26tNLwZQ/viewform).  
* Note: We cannot guarantee any space for groups that have not formally requested through this form. 
* We suggest teams have an agenda planned from 10:30am to afternoon or until your team dinner. 
* **Please note: The deadline to complete the form is February 20.**

#### What flexibility do we have?
The activities during the day is completely up to the team/manager. Some teams do bonding activities, others do work stuff. It is up to team leads to decide what is best for their group. Dinner will be planned by the Contribute team. 

#### Can we plan our own excursions?
Definitely, each team has $50 per person to spend on activities during the day. Anything in excess of this budget will be taken out of the team's budget. If you would like to plan one, we can set you up with our local agency to help you plan your team outing. Please let us know in the form what kind of activity you’d like to do, the rough budget, and headcount.

#### Can I bring in a guest speaker or workshop host?
Yes, please let us know your plans and note that any associated costs will need to be from your team's budget and requests submitted by February 20. Our local resources can help you find local leaders/ speakers. *We cannot accept any speakers that will require air travel or lodging*. 

#### What should managers do about team dinners?
In an effort to alleviate the burden of finding a suitable restaurant within budget we have pre-booked and assigned groups restaurants. Note that due to how large our group is this year, how time consuming small bookings are, and how rapidly the event is approaching, we had to make most team dinners for 50 or more people. Most orgs got broken up into two or more parts, with many of the smaller teams grouped together. Teams can choose to sit with their smaller orgs or intermix with the larger group. 

We will send a follow-up email with more details on team dinners and reservations, so hang tight.

#### Are SOs welcome? 
* Team leads will decide whether SOs will join Team Time events. 
* For the team dinner, we have included SOs in the budgeted amount, so team leads should please plan accordingly. 

#### What do community members, speakers, and other guests do during Team Time?
* All invited guests (e.g. Core team and Heroes) are the responsibility of the person who invited them. Please make sure they are included in your team’s dinner, and you communicate what aspects of the rest of Team Time they’re welcome to join. 
* If you are hosting a speaker, you are responsible for including that speaker in your team dinner. You do not need to invite them to your team meeting, but you should communicate to them on what to expect, when to be at a certain place, etc.

### Final party

![Into the Woods](/images/gitlab-contribute-into-woods.png){: .shadow.medium.center}

Inspired by Prague's rich history, folk tales, and castles, what better way to immerse ourselves in a fairytale city than go... _Into the Woods_! Our final party is set to be an enchanted evening, and costumes/accessories/themed attire is encouraged.  

## What expenses are covered for team members?

### Airfare or train tickets
Please see [Booking travel](#booking-travel) above.

#### Is checked luggage reimbursable? 
On TripActions, most airlines provide an option to pay extra for checked luggage. GitLab will reimburse the cost for adding one checked bag at a standard weight (only if the existing booking doesn't permit checked luggage), up to a maximum $100 USD total.   

If you booked outside of TripActions and the booking does not include any checked luggage, please feel free to expense the cost of adding one checked bag at a standard weight. If possible, please try to do so ahead of time, as it is often far more costly to do this when you reach the airport. 

### Getting to and from your home airport
Parking and mileage*/taxi/public transport to and from your home airport/train station can be expensed up to $80 USD total (this is on top of the limit set for flight costs in your region). 

*Please see [Spending company money](/handbook/spending-company-money/#expenses) for guidance regarding expensing mileage.

If you are extending your trip and traveling earlier or later than the official Contribute dates, you may still expense these travel costs associated with getting to/from your home airport on your traveling days.

Please use the `Prague Contribute 2020` classification in Expensify.

### Getting to and from the Contribute hotel

#### Arriving by air
GitLab will arrange transportation to and from Prague airport with scheduled group transfers. If you are arriving before March 22nd or leaving after March 27th we **won't reimburse you** for your transport to or from the airport in Prague, unless you have an exception explicitly agreed upon with the Contribute team. 

### Arriving by train
Watch this space – instructions coming soon!

#### Exceptions
- If you are coming in early as part of Support, the Contribute event team, or as a Contribute Ambassador, you may expense your transport to the Contribute hotel with the `Prague Contribute 2020` classification in Expensify.
- Those coming in for extended team meetings should mark those transportation expenses towards their team when expensing, rather than using the Contribute classification. 
- If you have to leave before March 27th, you may expense up to $25 to get to Prague airport as long as there are no transfers booked for you. 
- If you choose not to make use of GitLab-arranged transfers on your arrival/ departure date you **cannot expense** your transport. 

**Example 1**: You are flying in early to Prague to spend a few days sightseeing before arrivals day. You may expense your transport to your home airport, but not transport on arrival in Prague. You will need to make your own way to the Contribute hotel at your own expense. On departures day, GitLab will arrange and pay for a group transfer from the hotel to the airport. When you arrive back in your home country, you may expense your transport home from the airport.

**Example 2**: You arrive on arrivals day in Prague, but are staying on afterwards to travel elsewhere in Europe. You may expense your transport to your home airport. GitLab will arrange and pay for a group transfer to the hotel on arrival in Prague. You will then need to make your own way to the airport (wherever you are flying home from) at your own expense. When you arrive back in your home country, you may expense your transport home from the airport.

### Passport costs
If you need to get a passport or renew it *specifically* for traveling to Contribute, you can expense up to $75 USD to cover the cost of this process. If the costs are higher, GitLab will only reimburse up to $75 USD.

To take advantage of this perk, email `contribute@gitlab.com` with details for approval and include your manager in cc. Once approved, submit for expense reimbursement and mention that the expense has been approved.

### Visa application costs
Please [check first if you need a visa to visit Prague](/handbook/people-group/visas/#arranging-a-visa-for-travel-)! 

If you do need one, you may expense the visa application fee, transport to the embassy or visa application center, courier fees for return of your passport, and accommodation if you need to travel to another city to make your application. Please use the `Prague Contribute 2020` classification in Expensify.

Please take care to note what documents are needed for your visa application and always follow official guidance from the embassy or visa application center regarding timelines. **GitLab will not reimburse you for additional trips to apply if you haven't prepared adequately.**

### Accommodation
Hotel nights/tax have already been paid for by GitLab during Contribute. Each person will need to check in and provide a credit card for incidentals, but won't be charged unless there are any (e.g. room service). Basic internet is included in the room. When sharing a room, usually the first team member to arrive will be prompted to put down their card. Communicate with your roommate and discuss how to split any bill that may arise from room incidentals.

### Food and drink 

#### Meals
GitLab has arranged meals for everyone attending on the following dates: Monday, March 23rd - Thursday, March 26th. If you choose to not consume what we have prearranged or go out on your own, you cannot expense separate items/ meals. 

Example: We will have coffee at the hotel every day; if you decide to buy a coffee offsite that will not be expensable. 

On arrival and departure days (March 22 and 27) you may expense up to $65 USD towards meals. This $65 USD is for each day and applies to GitLab team members only. If you are traveling to Contribute on different days, you may expense this amount on your travel days only.

Example: As your Department is meeting early on March 22, you are traveling to Prague on March 20 to get acclimated with the new time zone. Then, after Contribute, you are leaving to spend one week in Greece until April 14th. You would still be able to expense $ 65 USD in meals on March 20 and April 14.

#### Drinks
GitLab will have a few hosted bars. If you order something outside of these hours or off menu these drink cannot be expensed or put on the GitLab tab. 

If your team has a happy hour or drinks with **your team dinner that will be handled by your team lead** – please ask them for their parameters on ordering. 

### What's **NOT** Covered
* Anything not listed in the [What expenses are covered](#what-expenses-are-covered-for-team-members) section. When in doubt, go with the assumption that it can't be expensed, and use your best judgment.
* Accommodation on any additional days outside the program (unless you are a Contribute Ambassador or Support team member joining early or staying on to provide coverage, or otherwise have prior permission from the Contribute team)
* Food on any additional days you stay outside the program or offsite (apart from the team dinners)
* Room service, mini/bar, late night eats/drinks, etc. (i.e. food and beverage outside of what is officially being provided during Contribute)
* Meals and beverages, activities, or excursions for any guest joining you without a Contribute ticket
* Rollaway bed at the hotel
* Transportation costs that aren't to/from the airport and hotel (e.g. do not expense cab rides from after-hours activities back to the hotel)
* Laundry
* Incidentals at the hotel (each person needs to put down their own card)
* Any snacks or souvenirs you pick up during excursions or offsite events unless provided by our team 
* Your costume or outfit for the [final party](#final-party)
* Additional visits to the embassy or visa application center for your country as a result of not being prepared with the correct documention or attempting to apply too soon
* Global Entry or equivalent
* Transportation to/from Prague hotel if you're traveling with an unregistered guest. Only registered guests can use the shuttles arranged by GitLab. 
* **Please note if you get anything delivered to the hotel, they charge a handling fee for all packages received.** This expense will be charged to your room.

___
We want to ensure everyone has a safe and successful week and that everyone feels like they are getting their needs met, so if you have any questions about the policies above please reach out to contribute@gitlab.com.

